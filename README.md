# Eye Focus Task Tracker

The Eye Focus Task Tracker is a project to analyze learning behaviour of a student who is working on electronic tasks of the [ONYX testsuite](https://www.bps-system.de/onyx-pruefungsplattform/). The application uses data of the pupil core eye tracking glasses of [pupil labs](https://pupil-labs.com/) to analyze if a student is focussing a task (or its elements) of the [ONYX testsuite](https://www.bps-system.de/onyx-pruefungsplattform/). It is mandatory that qr codes and ArUco marker have been added to the ONYX tasks by the project [onyx-interaction-tracker](https://gitlab.com/johannesctrl/onyx-interactions-tracker).

## Workflow

The following image shows how the tracker program works. It gets the video input of the Pupil Core and finds the qr codes and ArUco marker and their position in the image. By the position of the qr codes and the ArUco marker it can calculate the area of a task and its elements. After that the eye gaze's position of the student, calculated by the Pupil Core, is matched with the area of the task and its elements. By that the program can tell if the student is looking at a task or its elements.

![description](readme-resource/show2.jpg)

## Application

To cover multiple functionalities the project is created as a single tkinter application. The application provides the following variations to scan:
* Live Webcam (Debugging): Used for debugging purposes if a Pupil Core hardware is not available. Uses the video input of a webcam or another video capture device. The gaze position of the student can be simulated by mouse click.
* Live Pupil Core: Reads video and gaze of the Pupil Core in realtime. The software Pupil Capture is needed to create video and gaze before the Eye Focus Task Tracker analyzes the data.
* Local Pupil Data: Reads video and gaze of the stored data of a recording, recorded by the software Pupil Capture.

![application](readme-resource/show.jpg)