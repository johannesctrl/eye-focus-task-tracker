class Point:
    def __init__(self, x, y):
        self.x = self.roundNumber(x)
        self.y = self.roundNumber(y)

    def __repr__(self):
        return "(" + str(self.x) + "," + str(self.y) + ")"

    def roundPoint(self):
        self.x = int(round(self.x))
        self.y = int(round(self.y))

    def roundNumber(self, z):
        return int(round(z))