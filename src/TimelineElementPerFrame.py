import json


class TimelineElementPerFrame:
    """
    Diese Klasse bildet Instanzen eines TimelineElementes pro Frame. Jeder Frame soll ein TimelineElement
    besitzen. So ein TimelineElement beinhaltet alle wichtigen Attribute für ein REST-API-Request.
    """

    def __init__(self):
        self.userId = None
        self.studyName = None
        self.startTimestamp = None
        self.timestamp = None
        self.taskId = None
        self.elementId = None

    def toJson(self):
        data = {
            "userId": str(self.userId),
            "studyName": str(self.studyName),
            "startTimestamp": str(self.startTimestamp),
            "timestamp": str(self.timestamp),
            "taskId": str(self.taskId),
            "taskElementId": str(self.elementId)
        }

        json_data = json.dumps(data)
        # print("toJson:")
        # print(json_data)
        return json_data

    def toDict(self):
        data = {
            "userId": str(self.userId),
            "studyName": str(self.studyName),
            "startTimestamp": str(self.startTimestamp),
            "timestamp": str(self.timestamp),
            "taskId": str(self.taskId),
            "taskElementId": str(self.elementId)
        }
        return data