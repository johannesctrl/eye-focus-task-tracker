import numpy as np
import cv2


class Undistortion:
    def __init__(self, resolution):
        self.resolution = resolution  # (x, y) tupel
        self.D, self.K = self.calculate_D_and_K()

    def calculate_D_and_K(self):
        fisheye_properties = {
            "(1920, 1080)": {
                "dist_coefs": [
                    [
                        -0.13648546769272826,
                        -0.0033787366635030644,
                        -0.002343859061730869,
                        0.001926274947199097,
                    ]
                ],
                "camera_matrix": [
                    [793.8052697386686, 0.0, 953.2237035923064],
                    [0.0, 792.3104221704713, 572.5036513432223],
                    [0.0, 0.0, 1.0],
                ],
                "cam_type": "fisheye"
            }
        }

        D = fisheye_properties[f"({self.resolution[0]}, {self.resolution[1]})"]["dist_coefs"]
        D = np.array(D)
        K = fisheye_properties["(1920, 1080)"]["camera_matrix"]
        K = np.array(K).reshape(3, 3)

        return D, K

    def undistort_image(self, img):
        """
        Undistortes an image based on the camera model.
        :param img: Distorted input image
        :return: Undistorted image
        """
        R = np.eye(3)

        map1, map2 = cv2.fisheye.initUndistortRectifyMap(
            np.array(self.K),
            np.array(self.D),
            R,
            np.array(self.K),
            self.resolution,
            cv2.CV_16SC2,
        )

        undistorted_img = cv2.remap(
            img,
            map1,
            map2,
            interpolation=cv2.INTER_LINEAR,
            borderMode=cv2.BORDER_CONSTANT,
        )

        return undistorted_img

    def undistort_gaze(self, global_norm_pos_x, global_norm_pos_y):
        """
        :param global_norm_pos_x: x value between 0 and 1
        :param global_norm_pos_y: y value between 0 and 1
        :return: x and y position of the gaze in pixel as tuple
        """
        pixel_pos = self.denormalize(
            (global_norm_pos_x, global_norm_pos_y), self.resolution, flip_y=True
        )
        undistorted3d = self.unproject_points(pixel_pos)
        undistorted2d = self.project_points(
            undistorted3d, use_distortion=False
        )
        width, height = self.resolution
        x, y = (int(undistorted2d[0][0]), int(undistorted2d[0][1]))
        if x > width:
            x = width
        if y > height:
            y = height
        if x < 0:
            x = 0
        if y < 0:
            y = 0
        return x, y

    def project_points(self, object_points, rvec=None, tvec=None, use_distortion=True):
        """
        Projects a set of points onto the camera plane as defined by the camera model.
        :param object_points: Set of 3D world points
        :param rvec: Set of vectors describing the rotation of the camera when recording
            the corresponding object point
        :param tvec: Set of vectors describing the translation of the camera when
            recording the corresponding object point
        :return: Projected 2D points
        """
        skew = 0

        input_dim = object_points.ndim

        object_points = object_points.reshape((1, -1, 3))

        if rvec is None:
            rvec = np.zeros(3).reshape(1, 1, 3)
        else:
            rvec = np.array(rvec).reshape(1, 1, 3)

        if tvec is None:
            tvec = np.zeros(3).reshape(1, 1, 3)
        else:
            tvec = np.array(tvec).reshape(1, 1, 3)

        if use_distortion:
            _D = self.D
        else:
            _D = np.asarray([[1.0 / 3.0, 2.0 / 15.0, 17.0 / 315.0, 62.0 / 2835.0]])

        image_points, jacobian = cv2.fisheye.projectPoints(
            object_points, rvec, tvec, self.K, _D, alpha=skew
        )

        if input_dim == 2:
            image_points.shape = (-1, 2)
        elif input_dim == 3:
            image_points.shape = (-1, 1, 2)
        return image_points

    def unproject_points(self, pts_2d, use_distortion=True, normalize=False):
        """
        Undistorts points according to the camera model. cv2.fisheye.undistortPoints
        does *NOT* perform the same unprojection step the original cv2.unprojectPoints
        does. Thus we implement this function ourselves.
        :param pts_2d, shape: Nx2
        :return: Array of unprojected 3d points, shape: Nx3
        """

        pts_2d = np.array(pts_2d, dtype=np.float32)

        # Delete any posibly wrong 3rd dimension
        if pts_2d.ndim == 1 or pts_2d.ndim == 3:
            pts_2d = pts_2d.reshape((-1, 2))

        eps = np.finfo(np.float32).eps
        # print("hier:", K, type(K))
        f = np.array((self.K[0, 0], self.K[1, 1])).reshape(1, 2)
        c = np.array((self.K[0, 2], self.K[1, 2])).reshape(1, 2)
        if use_distortion:
            k = self.D.ravel().astype(np.float32)
        else:
            k = np.asarray(
                [1.0 / 3.0, 2.0 / 15.0, 17.0 / 315.0, 62.0 / 2835.0], dtype=np.float32
            )

        pi = pts_2d.astype(np.float32)
        pw = (pi - c) / f

        theta_d = np.linalg.norm(pw, ord=2, axis=1)
        theta = theta_d
        for j in range(10):
            theta2 = theta ** 2
            theta4 = theta2 ** 2
            theta6 = theta4 * theta2
            theta8 = theta6 * theta2
            theta = theta_d / (
                    1 + k[0] * theta2 + k[1] * theta4 + k[2] * theta6 + k[3] * theta8
            )

        scale = np.tan(theta) / (theta_d + eps)

        pts_2d_undist = pw * scale.reshape(-1, 1)

        pts_3d = cv2.convertPointsToHomogeneous(pts_2d_undist)
        pts_3d.shape = -1, 3

        if normalize:
            pts_3d /= np.linalg.norm(pts_3d, axis=1)[:, np.newaxis]
        return pts_3d

    def denormalize(self, pos, size, flip_y=False):
        """
        denormalize
        """
        width, height = size
        x = pos[0]
        y = pos[1]
        x *= width
        if flip_y:
            y = 1 - y
        y *= height
        # print(x, y)
        return x, y

