import csv


class GazePositionsCSVFile:
    def __init__(self, path):
        self.path = path

    def import_gaze(self):
        """
        Get two lists (x and y) of the gaze position.
        """
        x = []
        y = []
        t = []
        world_index = -1
        with open(self.path) as csv_file:
            csv_reader = csv.reader(csv_file)
            next(csv_reader)
            for line in csv_reader:
                # print(line)
                if (world_index == int(line[1])):
                    next(csv_reader)
                    # print(world_index)
                else:
                    world_index += 1
                    timestamp = float(line[0])
                    world_index = int(line[1])
                    norm_pos_x = float(line[3])
                    norm_pos_y = float(line[4])
                    x.append(norm_pos_x)
                    y.append(norm_pos_y)
                    t.append(timestamp)
        return x, y, t
