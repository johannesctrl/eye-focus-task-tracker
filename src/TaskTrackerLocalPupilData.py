import requests

from Eyes import Eyes
from GazeTLVFile import GazeTLVFile
from TaskRecords import TaskRecords
from TimelineElementsPerFrame import TimelineElementsPerFrame
from TasksPerFrame import TasksPerFrame

import cv2
import time
import tkinter as tk
from pyzbar.pyzbar import decode
from pyzbar.pyzbar import ZBarSymbol
from queue import Queue
from threading import Thread

from Undistortion import Undistortion


class TaskTrackerLocalPupilData:
    def __init__(self, recording_folder, userId, httpRequestPostUrl, lens_type):
        self.recording_folder = recording_folder
        self.userId = userId
        self.httpRequestPostUrl = httpRequestPostUrl
        self.lens_type = lens_type
        self._sentinel = object()

    def open_local_pupil_data(self):
        video_path = None
        x_list, y_list = None, None
        timestamp_list = None
        start_time_system = None
        start_time_synced = None
        if self.lens_type == "narrow":
            video_path = self.recording_folder.world_mp4_path
            x_list, y_list, timestamp_list = self.recording_folder.gaze_positions_csv.import_gaze()
            start_time_system = float(self.recording_folder.info_player_json_dict["start_time_system_s"])  # System Time at recording start
            start_time_synced = float(self.recording_folder.info_player_json_dict["start_time_synced_s"])  # Pupil Time at recording start
        if self.lens_type == "wide":
            video_path = self.recording_folder.iMotions_video_path
            tlv_file = self.recording_folder.iMotions_tlv_file
            x_list = tlv_file.get_column_by_name("Gaze2dX")
            y_list = tlv_file.get_column_by_name("Gaze2dY")
            timestamp_list = tlv_file.get_column_by_name("GazeTimeStamp")
            start_time_system = self.recording_folder.get_iMotions_info_dict()["Start Time (System)"]
            start_time_synced = self.recording_folder.get_iMotions_info_dict()["Start Time (Synced)"]

        # video
        capture = cv2.VideoCapture(video_path)
        res_x = int(capture.get(3))
        res_y = int(capture.get(4))
        print("Video resolution is: " + str(res_x) + "x" + str(res_y))

        # eyes
        eyes = Eyes()

        task_records = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.

        # frames
        frame_number = 0
        fps = 0
        prev_frame_time = 0

        while capture.isOpened:  # existiert die Datei, bzw. der Livestream (noch)
            success, frame = capture.read()
            if not success:
                print("Video has ended.")
                capture.release()
                cv2.destroyWindow("frame")
                break

            if self.lens_type == "narrow":
                eyes.set_x(x_list[frame_number] * res_x)
                eyes.set_y(y_list[frame_number] * res_y)
            if self.lens_type == "wide":
                eyes.set_x(x_list[frame_number])
                eyes.set_y(y_list[frame_number])

            print(eyes.get_coo())

            dframe = frame.copy()

            window_size = 80

            # qr scanner
            decode_frame = decode(frame, symbols=[ZBarSymbol.QRCODE])
            task_list_per_frame = TasksPerFrame(frame_number)
            current_timestamp = self.recording_folder.convert_pupil_time_to_system_time_all(timestamp_list[frame_number], float(start_time_system), float(start_time_synced))
            task_list_per_frame.decodeFrame(decode_frame, current_timestamp)

            # tasks focused?
            task_list_per_frame.checkIfFocused_ForAllTasks(eyes)

            # save results
            task_records.importTasksPerFrame(task_list_per_frame, frame_number)

            # draw
            task_records.drawTasks(dframe)
            task_list_per_frame.drawRecsQrCodes_ForAllTasks(dframe)
            task_list_per_frame.drawRect_ForAllTasks(dframe)
            task_list_per_frame.drawRectPoint_ForAllTasks(dframe)
            task_list_per_frame.drawID_ForAllTasks(dframe)
            cv2.circle(dframe, (eyes.x, eyes.y), 10, (255, 0, 0), -1)  # visualize gaze
            cv2.putText(dframe, "fps=" + str(fps), (res_x - 500, res_y - 10), 1, 1.4, (140, 22, 99), 2)
            draw_controls(dframe)
            cv2.imshow("frame", scale_image(dframe, window_size))

            # controls
            pressedKey = cv2.waitKey(1) & 0xFF
            if pressedKey == ord('q'):  # close
                cv2.destroyWindow("frame")
                capture.release()
                break

            # FPS
            new_frame_time = time.time()
            fps = 1 / (new_frame_time - prev_frame_time)
            prev_frame_time = new_frame_time
            fps = int(fps)
            frame_number += 1

    def consumer(self, in_q):
        while True:
            data = in_q.get()
            # print(data)
            json_data = None
            url = None
            if data is self._sentinel:
                in_q.put(self._sentinel)
                break
            else:
                url = data[0]
                json_data = data[1]
                print(json_data)
            send_to_las2peer(self.httpRequestPostUrl, json_data)
            in_q.task_done()

    def run_eye_tracking_in_threads(self):
        """
        This method is called to create a producer and a consumer thread. The consumer thread
        is for sending JSON-Statements to the Las2Peer-Service. The producer function is e.g.
        openLiveWebcam, etc. This method exists to call all functions that analyse the video
        and gaze material as producer functions.
        """
        producer = self.open_live_webcam
        consumer = self.consumer

        q = Queue()
        t1 = Thread(target=producer, args=(q,))
        t2 = Thread(target=consumer, args=(q,))
        t1.start()
        t2.start()
        t1.join()
        t2.join()


def draw_controls(dframe):
    wOffset = 20
    h, w, _ = dframe.shape
    # print("width="+ str(w), "height=" + str(h))
    color = (255, 255, 0)
    quit_content = "Press q to quit"
    changeCamera_content = "Press numbers from 0 to 4 to change camera input"
    cv2.putText(dframe, changeCamera_content, (wOffset, h - 90), 1, 1.9, color, 1)
    cv2.putText(dframe, quit_content, (wOffset, h - 60), 1, 1.9, color, 1)

def scale_image(img, scale_percent):
    """ Scales an image to x percent """
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    return img

def send_to_las2peer(url, data):
    print("sendToL2P")
    print("url:", url)
    print("data:", data)
    r = requests.post(url=url, data=data)
    print(r)
    r = requests.post(url=url, data=data, timeout=5.0)
    if r.ok:
        print("Posting Data to L2P was successful.")
    else:
        print("Posting Data to L2P wasn't successful.")
    print("END sendtoL2P")




