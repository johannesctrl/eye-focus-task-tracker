class ElementRecord():
    def __init__(self, id):
        self.id = id
        self.timeline = []
        self.focusedCounter = 0
        self.unfocusedCounter = 0

    def countFocusedFrames(self):
        counter = 0
        for record in self.timeline:
            if record[1] == True:
                counter += 1
        # count = self.timeline.count((True, True))
        return counter

    def countUnfocusedFrames(self):
        counter = 0
        for record in self.timeline:
            if record[1] == False:
                counter += 1
        # count = self.timeline.count((True, False))
        return counter
