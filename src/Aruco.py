from Point import Point

class Aruco:
    def __init__(self, id, rect):
        self.id = id
        self.rect = rect

    def getCenter(self):
        """Calculates the center of the aruco's rectangle"""
        p1 = self.rect[0]
        p4 = self.rect[3]
        x = (p1.x + p4.x) / 2
        y = (p1.y + p4.y) / 2
        pCenter = Point(x, y)
        return pCenter