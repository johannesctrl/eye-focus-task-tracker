import os

from RecordingFolder import RecordingFolder


class SessionFolder:  # session_01 z.B. Pilotstudie
    def __init__(self, path, main_folder):
        self.main_folder = main_folder
        self.path = path
        self.name = os.path.basename(os.path.dirname(path))
        self.recording_folder_list = self.create_recording_folders()

    def create_recording_folders(self):
        # print("create_recording_folders...")
        recording_folder_list = []
        for recording_folder_name in os.listdir(self.path):
            recording_folder_path = self.path + recording_folder_name
            recording_folder = RecordingFolder(recording_folder_path, recording_folder_name, self)
            recording_folder_list.append(recording_folder)
        return recording_folder_list
