import os
import csv
import json

from GazePositionsCSVFile import GazePositionsCSVFile
from GazeTLVFile import GazeTLVFile


class RecordingFolder:
    def __init__(self, path, name, session_folder):
        self.path = path
        self.name = name
        self.session_folder = session_folder
        self.valid = None
        self.narrow_lens_valid = None
        self.exports_folder_path = None
        self.iMotions_folder_path = None  # (for wide angle lens)
        self.iMotions_video_path = None
        self.iMotions_info_dict = None  # (for wide angle lens)
        self.iMotions_tlv_file = None
        self.world_mp4_path = None  # (for narrow angle lens)
        self.info_player_json_path = None  # (for narrow angle lens)
        self.info_player_json_dict = None
        self.gaze_positions_csv = None  # (for narrow angle lens)
        self.routine()

    def routine(self):
        # wide angle lens
        try:
            self.exports_folder_path = self.get_first_exports_folder_path()
            self.iMotions_folder_path = self.get_iMotions_folder_path()
            self.iMotions_video_path = self.get_iMotions_video_path()
            self.iMotions_info_dict = self.get_iMotions_info_dict()
            self.iMotions_tlv_file = self.get_iMotions_tlv_file()
            self.valid = True
            print("wide angle lens: all needed files detected")
        except Exception as e:
            print(e)
            self.valid = False

        # narrow angle lens
        try:
            self.exports_folder_path = self.get_first_exports_folder_path()
            self.world_mp4_path = self.get_world_mp4_path()
            self.info_player_json_path = self.get_info_player_json_path()
            self.info_player_json_dict = self.get_info_player_json_dict()
            self.gaze_positions_csv = self.get_gaze_positions_csv()
            self.narrow_lens_valid = True
            print("narrow angle lens: all needed files detected")
        except Exception as e:
            print(e)
            self.narrow_lens_valid = False

    def get_iMotions_tlv_path(self):
        return self.iMotions_folder_path + "gaze.tlv"

    def get_iMotions_tlv_file(self):
        return GazeTLVFile(self.get_iMotions_tlv_path())

    def get_iMotions_video_path(self):
        return self.iMotions_folder_path + "scene.mp4"

    def get_world_mp4_path(self):
        return self.path + "/world.mp4"

    def get_first_exports_folder_path(self):
        # print("getFirstExportedFolder...")
        exports_path = self.path + "/exports/"
        first_exports_folder_name = os.listdir(exports_path)[0]
        exports_folder_path = exports_path + first_exports_folder_name + "/"
        return exports_folder_path

    def get_gaze_positions_csv(self):
        path = self.get_first_exports_folder_path() + "/gaze_positions.csv"
        return GazePositionsCSVFile(path)

    def get_iMotions_folder_path(self):
        # print("get_iMotions_folder_path...")
        for folderName in os.listdir(self.exports_folder_path):
            if folderName[0:8] == "iMotions":
                iMotionsFolderPath = self.exports_folder_path + folderName + "/"
                # print("iMotionsFolderPath:", iMotionsFolderPath)
                return iMotionsFolderPath

    def get_info_player_json_path(self):
        return self.path + "/info.player.json"

    def get_info_player_json_dict(self):
        print("get_info_player_json_dict")
        with open(self.get_info_player_json_path()) as json_file:
            data = json.load(json_file)
            print("Type:", type(data))
            return data

    def get_iMotions_info_dict(self):
        # print("getiMotions_info")
        csvPath = str(self.iMotions_folder_path) + "iMotions_info.csv"

        with open(csvPath) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=",")
            header = next(csv_reader)
            dict = {}
            for line in csv_reader:
                key = line[0]
                value = line[1]
                dict[key] = value
            return dict

    def convert_pupil_time_to_system_time_all(self, pupil_timestamp, start_time_system, start_time_synced):
        offset = start_time_system - start_time_synced # Calculate the fixed offset between System and Pupil Time
        pupiltime_in_systemtime = pupil_timestamp + offset # Add the fixed offset to the timestamp(s) we wish to convert
        pupiltime_in_systemtime_ms = int(pupiltime_in_systemtime * 1000)
        # print("convertPupilTimeToSystemTime:", pupil_timestamp, start_time_system, start_time_synced, pupiltime_in_systemtime_ms)
        return pupiltime_in_systemtime_ms

    def convert_pupil_time_to_system_time(self, pupil_timestamp):
        print("convertPupilTimeToSystemTime")
        dict = self.get_iMotions_info_dict()

        start_time_system = float(dict["Start Time (System)"])  # System Time at recording start
        start_time_synced = float(dict["Start Time (Synced)"])  # Pupil Time at recording start
        # print("start_time_system: " + str(start_time_system))
        # print("start_time_synced: " + str(start_time_synced))

        # Calculate the fixed offset between System and Pupil Time
        offset = start_time_system - start_time_synced

        # Add the fixed offset to the timestamp(s) we wish to convert
        pupiltime_in_systemtime = pupil_timestamp + offset

        # Using the datetime python module, we can convert timestamps
        # stored as seconds represented by floating point values to a
        # more readable datetime format.
        # pupil_datetime = datetime.fromtimestamp(pupiltime_in_systemtime).strftime("%Y-%m-%d %H:%M:%S.%f")
        # print(pupil_datetime)
        # example output: '2018-08-02 15:16:08.199800'

        # Hint: you can also copy and paste timestamps into various websites that convert them
        # to the readable date time format!
        pupiltime_in_systemtime_ms = int(pupiltime_in_systemtime * 1000)
        print(pupiltime_in_systemtime_ms)
        return pupiltime_in_systemtime_ms