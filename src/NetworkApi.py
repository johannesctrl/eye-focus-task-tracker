import zmq
from msgpack import packb, unpackb


class NetworkApi:
    def __init__(self, ip_address, req_port, bgr):
        self.ip_address = ip_address
        self.req_port = req_port
        self.bgr = bgr

        self.context = self.create_context(ip_address, req_port)
        self.req = self.create_req(self.context)
        self.sub = self.create_sub(self.req, self.context)

        self.notify(self.bgr)

    def create_context(self, ip_address, req_port):
        ip_address = "127.0.0.1"  # open a req port to talk to pupil; remote ip or localhost
        req_port = "50020"  # same as in the pupil remote gui
        context = zmq.Context()
        return context

    def create_req(self, context):
        req = context.socket(zmq.REQ)
        req.connect("tcp://{}:{}".format(self.ip_address, self.req_port))
        req.send_string("SUB_PORT")  # ask for the sub port
        return req

    def create_sub(self, req, context):
        sub_port = req.recv_string()
        sub = context.socket(zmq.SUB)  # open a sub port to listen to pupil
        sub.connect("tcp://{}:{}".format(self.ip_address, sub_port))
        sub.setsockopt_string(zmq.SUBSCRIBE, "frame.")
        sub.setsockopt_string(zmq.SUBSCRIBE, "gaze.")
        return sub

    def notify(self, frame_format):
        """Sends ``notification`` to Pupil Remote"""
        notification = {"subject": "frame_publishing.set_format",
                        "format": frame_format}
        topic = "notify." + notification["subject"]
        payload = packb(notification, use_bin_type=True)
        self.req.send_string(topic, flags=zmq.SNDMORE)
        self.req.send(payload)
        return self.req.recv_string()

    def recv_from_sub(self):
        """Recv a message with topic, payload.
        Topic is a utf-8 encoded string. Returned as unicode object.
        Payload is a msgpack serialized dict. Returned as a python dict.
        Any addional message frames will be added as a list
        in the payload dict with key: '__raw_data__' .
        """
        topic = self.sub.recv_string()
        payload = unpackb(self.sub.recv(), raw=False)
        extra_frames = []
        while self.sub.get(zmq.RCVMORE):
            extra_frames.append(self.sub.recv())
        if extra_frames:
            payload["__raw_data__"] = extra_frames
        return topic, payload

    def has_new_data_available(self):
        # Returns True as long subscription socket has received data queued for processing
        return self.sub.get(zmq.EVENTS) & zmq.POLLIN
