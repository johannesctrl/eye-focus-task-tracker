from ElementRecords import ElementRecords


class TaskRecord():
    """
    Diese Klasse beinhaltet alle gespeicherten Daten einer einzelnen Task und ist in der Klasse "TaskRecords" als Liste.
    """

    def __init__(self, id):
        self.id = id
        self.frameCounter = 1
        self.focusedFrameCounter = 1
        self.unfocusedFrameCounter = 1
        self.elementRecords = ElementRecords()
        self.timeline = []

    def toString(self):
        return "id = " + str(self.id) + ", frameCounter = " + str(self.frameCounter)
