import cv2
import time
import numpy as np
import requests
from pyzbar.pyzbar import decode
from pyzbar.pyzbar import ZBarSymbol
from queue import Queue
from threading import Thread

from NetworkApi import NetworkApi
from Eyes import Eyes
from TaskRecords import TaskRecords
from TasksPerFrame import TasksPerFrame
from TimelineElementsPerFrame import TimelineElementsPerFrame
from Undistortion import Undistortion


class TaskTrackerPupilCoreLive:
    def __init__(self, window_size, user_id, frame_format, ip_address, port_number, http_request_post_url, lens_type):
        self.window_size = window_size
        self.user_id = user_id
        self.frame_format = frame_format
        self.ip_address = ip_address
        self.port_number = port_number
        self.http_request_post_url = http_request_post_url
        self.lens_type = lens_type

        self._sentinel = object()

    def open_live_pupil_core(self, out_q):
        print("open_live_pupil_core")
        network_api = NetworkApi(self.ip_address, self.port_number, self.frame_format)

        _sentinel = object()
        global_norm_pos_x = 0
        global_norm_pos_y = 0
        gaze_pixel = None
        frame = None
        resolution = None

        eyes = Eyes()
        task_records = TaskRecords()  # save tasksPerFrame, recognized each frame
        timeline = TimelineElementsPerFrame(self.http_request_post_url, out_q)

        # timestamps
        start_timestamp_raw = time.time()  # in seconds but float
        start_timestamp_ms = int(start_timestamp_raw * 1000)

        # frames
        frame_number = 0
        fps = 0
        prev_frame_time = 0

        try:
            while True:
                while network_api.has_new_data_available():

                    topic, msg = network_api.recv_from_sub()

                    if topic.startswith("gaze."):  # get gaze

                        global_norm_pos_x, global_norm_pos_y = msg['norm_pos']

                    if topic.startswith("frame.") and msg["format"] != self.frame_format:
                        print(
                            f"different frame format ({msg['format']}); "
                            f"skipping frame from {topic}"
                        )
                        continue

                    if topic == "frame.world":  # get video
                        frame = np.frombuffer(
                            msg["__raw_data__"][0],
                            dtype=np.uint8).reshape(msg["height"], msg["width"], 3)
                        res_x = msg['width']
                        res_y = msg['height']
                        resolution = (res_x, res_y)
                if frame is not None and global_norm_pos_x is not 0:

                    if self.lens_type == "narrow":
                        gaze_pixel = (global_norm_pos_x, global_norm_pos_y)
                        eyes.set_x(int(gaze_pixel[0] * res_x))
                        eyes.set_y(int(res_y - gaze_pixel[1] * res_y))
                    if self.lens_type == "wide":
                        undistortion = Undistortion(resolution)
                        frame = undistortion.undistort_image(frame)
                        gaze_pixel = undistortion.undistort_gaze(global_norm_pos_x, global_norm_pos_y)
                        eyes.set_x(gaze_pixel[0])
                        eyes.set_y(gaze_pixel[1])


                    dframe = frame.copy()

                    # timestamps
                    current_timestamp_raw = time.time()  # in seconds but float
                    current_timestamp_ms = int(current_timestamp_raw * 1000)

                    # frames
                    frame_number += 1

                    # qr code scanner
                    decoded_frame = decode(frame, symbols=[ZBarSymbol.QRCODE])
                    tasks_per_frame = TasksPerFrame(frame_number)
                    tasks_per_frame.decodeFrame(decoded_frame, current_timestamp_ms)

                    # AruCo scanner
                    tasks_per_frame.decodeArucoMarkers(frame, dframe)
                    tasks_per_frame.assignArucosToTasks()
                    tasks_per_frame.createTaskElementPerFrames_ForAllTasks()

                    # focused?
                    tasks_per_frame.checkIfFocused_ForAllTasks(eyes)

                    # save results
                    task_records.importTasksPerFrame(tasks_per_frame, frame_number)
                    task_records.drawTasks(dframe)
                    timeline.importTasksPerFrame(tasks_per_frame, self.user_id, start_timestamp_ms, current_timestamp_ms)

                    # draw
                    tasks_per_frame.drawRecsQrCodes_ForAllTasks(dframe)
                    tasks_per_frame.drawRect_ForAllTasks(dframe)
                    tasks_per_frame.drawRectPoint_ForAllTasks(dframe)
                    tasks_per_frame.drawID_ForAllTasks(dframe)
                    ## arucos
                    tasks_per_frame.drawArucos_ForAllTasks(dframe)
                    tasks_per_frame.drawTaskElements_ForAllTasks(dframe)
                    ## gaze point
                    cv2.circle(dframe, (eyes.x, eyes.y), 10, (255, 0, 0), -1)  # visualisiere Eye-Tracking
                    ## text
                    cv2.putText(dframe, "fps=" + str(fps), (1920 - 500, 1080 - 10), 1, 1.4, (140, 22, 99), 2)
                    cv2.putText(dframe, "userId=" + str(self.user_id), (1920 - 500, 1080 - 30), 1, 1.4, (140, 22, 99), 2)
                    cv2.imshow("frame", scale_image(dframe, self.window_size))

                    # controls
                    pressed_key = cv2.waitKey(1) & 0xFF
                    if pressed_key == ord('q'):  # close
                        out_q.put(_sentinel)
                        cv2.destroyWindow("frame")
                        break

                    # fps
                    new_frame_time = time.time()
                    fps = 1 / (new_frame_time - prev_frame_time)
                    prev_frame_time = new_frame_time
                    fps = int(fps)

        except KeyboardInterrupt:
            print("KeyboardInterrupt")
            pass
        finally:
            print("destroyAllWindows")
            cv2.destroyAllWindows()

    def consumer(self, in_q):
        while True:
            data = in_q.get()
            print(data)
            json_data = None
            url = None
            if data is self._sentinel:
                in_q.put(self._sentinel)
                break
            else:
                url = data[0]
                json_data = data[1]
                print(json_data)
            send_to_las2peer(self.http_request_post_url, json_data)
            in_q.task_done()

    def run_eye_tracking_in_threads(self):
        """
        This method is called to create a producer and a consumer thread. The consumer thread
        is for sending JSON-Statements to the Las2Peer-Service. The producer function is e.g.
        openLiveWebcam, etc. This method exists to call all functions that analyse the video
        and gaze material as producer functions.
        """
        producer = self.open_live_pupil_core
        consumer = self.consumer

        q = Queue()
        t1 = Thread(target=producer, args=(q,))
        t2 = Thread(target=consumer, args=(q,))
        t1.start()
        t2.start()
        t1.join()
        t2.join()

def draw_controls(dframe):
    wOffset = 20
    h, w, _ = dframe.shape
    # print("width="+ str(w), "height=" + str(h))
    color = (255, 255, 0)
    quit_content = "Press q to quit"
    changeCamera_content = "Press numbers from 0 to 4 to change camera input"
    cv2.putText(dframe, changeCamera_content, (wOffset, h - 90), 1, 1.9, color, 1)
    cv2.putText(dframe, quit_content, (wOffset, h - 60), 1, 1.9, color, 1)


def scale_image(img, scale_percent):
    """ Scales an image to x percent """
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    return img


def send_to_las2peer(url, data):
    print("sendToL2P")
    print("url:", url)
    print("data:", data)
    r = requests.post(url=url, data=data)
    print(r)
    r = requests.post(url=url, data=data, timeout=5.0)
    if r.ok:
        print("Posting Data to L2P was successful.")
    else:
        print("Posting Data to L2P wasn't successful.")
    print("END sendtoL2P")
