import requests

from Eyes import Eyes
from TaskRecords import TaskRecords
from TimelineElementsPerFrame import TimelineElementsPerFrame
from TasksPerFrame import TasksPerFrame

import cv2
import time
import tkinter as tk
from pyzbar.pyzbar import decode
from pyzbar.pyzbar import ZBarSymbol
from queue import Queue
from threading import Thread


class TaskTracker:
    def __init__(self, camIndex, videoRes, windowSize, userId, httpRequestPostUrl):
        self.camIndex = camIndex
        self.videoRes = videoRes
        self.windowSize = windowSize
        self.userId = userId
        self.httpRequestPostUrl = httpRequestPostUrl
        self._sentinel = object()

    def open_live_webcam(self, out_q):
        # camera settings
        capture = cv2.VideoCapture(self.camIndex, cv2.CAP_DSHOW)
        xRes, yRes = self.videoRes
        capture.set(3, xRes)  # ID bei set für die Breite ist 3
        capture.set(4, yRes)
        res_x = int(capture.get(3))
        res_y = int(capture.get(4))
        if res_x == 0 and res_y == 0:
            tk.messagebox.showinfo("Camera Index", "No Camera connected at this index.")
            return

        eyes = Eyes()  # Die Augen sind per default in der Mitte der Bild-Aufnahme

        taskRecords = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
        timeline = TimelineElementsPerFrame(self.httpRequestPostUrl, out_q)

        # timestamps
        startTimestamp_raw = time.time()  # in seconds but float
        startTimestamp_ms = int(startTimestamp_raw * 1000)

        # frames
        frameNumber = 0
        fps = 0
        prev_frame_time = 0
        frameTimeline = []

        while capture.isOpened:
            success, frame = capture.read()
            if not success:
                tk.messagebox.showinfo("Camera", "The camera is disconnected.")
                capture.release()
                cv2.destroyWindow("frame")
                return
            dframe = frame.copy()

            # Timestamps
            cTimestamp_raw = time.time()  # in seconds but float
            cTimestamp_ms = int(cTimestamp_raw * 1000)

            frameNumber += 1
            frameTimeline.append(frameNumber)

            cv2.circle(dframe, (int(eyes.x), int(eyes.y)), 10, (255, 0, 0), -1)  # visualisiere Eye-Tracking

            # QR-Scanner
            decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
            tasksPerFrame = TasksPerFrame(frameNumber)
            tasksPerFrame.decodeFrame(decodedFrame, cTimestamp_ms)

            # AruCo-Scanner
            tasksPerFrame.decodeArucoMarkers(frame, dframe)
            tasksPerFrame.assignArucosToTasks()
            tasksPerFrame.createTaskElementPerFrames_ForAllTasks()

            # Check if Tasks are focused
            tasksPerFrame.checkIfFocused_ForAllTasks(eyes)

            # Save Results
            taskRecords.importTasksPerFrame(tasksPerFrame, frameNumber)
            taskRecords.drawTasks(dframe)
            timeline.importTasksPerFrame(tasksPerFrame, self.userId, startTimestamp_ms, cTimestamp_ms)

            # draw
            tasksPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
            tasksPerFrame.drawRect_ForAllTasks(dframe)
            tasksPerFrame.drawRectPoint_ForAllTasks(dframe)
            tasksPerFrame.drawID_ForAllTasks(dframe)

            tasksPerFrame.drawArucos_ForAllTasks(dframe)
            tasksPerFrame.drawTaskElements_ForAllTasks(dframe)

            cv2.putText(dframe, "fps=" + str(fps), (1920 - 500, 1080 - 10), 1, 1.4, (140, 22, 99), 2)
            draw_controls(dframe)
            cv2.putText(dframe, "userId=" + str(self.userId), (1920 - 500, 1080 - 30), 1, 1.4, (140, 22, 99), 2)
            draw_controls(dframe)
            cv2.imshow("frame", scale_image(dframe, self.windowSize))

            # controls
            pressedKey = cv2.waitKey(1) & 0xFF
            if pressedKey == ord('q'):  # close
                out_q.put(self._sentinel)
                cv2.destroyWindow("frame")
                break

            def mousePoints(event, x, y, flags, params):
                if event == cv2.EVENT_LBUTTONDOWN:
                    eyes.set_x(x / (self.windowSize / 100))
                    eyes.set_y(y / (self.windowSize / 100))
                    print(x, y)

            cv2.setMouseCallback("frame", mousePoints)

            # fps
            new_frame_time = time.time()
            fps = 1 / (new_frame_time - prev_frame_time)
            prev_frame_time = new_frame_time
            fps = int(fps)

        out_q.put(self._sentinel)
        capture.release()

    def consumer(self, in_q):
        while True:
            data = in_q.get()
            print(data)
            json_data = None
            url = None
            if data is self._sentinel:
                in_q.put(self._sentinel)
                break
            else:
                url = data[0]
                json_data = data[1]
                print(json_data)
            send_to_las2peer(self.httpRequestPostUrl, json_data)
            in_q.task_done()

    def run_eye_tracking_in_threads(self):
        """
        This method is called to create a producer and a consumer thread. The consumer thread
        is for sending JSON-Statements to the Las2Peer-Service. The producer function is e.g.
        openLiveWebcam, etc. This method exists to call all functions that analyse the video
        and gaze material as producer functions.
        """
        producer = self.open_live_webcam
        consumer = self.consumer

        q = Queue()
        t1 = Thread(target=producer, args=(q,))
        t2 = Thread(target=consumer, args=(q,))
        t1.start()
        t2.start()
        t1.join()
        t2.join()


def draw_controls(dframe):
    wOffset = 20
    h, w, _ = dframe.shape
    # print("width="+ str(w), "height=" + str(h))
    color = (255, 255, 0)
    quit_content = "Press q to quit"
    changeCamera_content = "Press numbers from 0 to 4 to change camera input"
    cv2.putText(dframe, changeCamera_content, (wOffset, h - 90), 1, 1.9, color, 1)
    cv2.putText(dframe, quit_content, (wOffset, h - 60), 1, 1.9, color, 1)


def scale_image(img, scale_percent):
    """ Scales an image to x percent """
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    return img


def send_to_las2peer(url, data):
    print("sendToL2P")
    print("url:", url)
    print("data:", data)
    r = requests.post(url=url, data=data)
    print(r)
    r = requests.post(url=url, data=data, timeout=5.0)
    if r.ok:
        print("Posting Data to L2P was successful.")
    else:
        print("Posting Data to L2P wasn't successful.")
    print("END sendtoL2P")
