import numpy as np
import shapely
import sympy

from Point import Point


class TaskElementPerFrame:
    def __init__(self, arucoPair, task):
        self.arucoPair = arucoPair
        self.task = task
        self.id = self.getId()
        self.arucoLeft, self.arucoRight = self.getArucoPos()
        self.rect = self.getRectParallel()
        self.focused = False

    def getPolygon(self):
        p0 = self.rect[0]
        p1 = self.rect[1]
        p2 = self.rect[2]
        p3 = self.rect[3]
        polygon = np.array([[p0.x, p0.y], [p1.x, p1.y], [p2.x, p2.y], [p3.x, p3.y]], np.int32)
        polygon = polygon.reshape((-1, 1, 2))
        return polygon

    def getId(self):
        return self.arucoPair[0].id

    def getArucoPos(self):
        """Sorts aruco marker pair from left to right"""
        aruco1 = self.arucoPair[0]
        aruco2 = self.arucoPair[1]
        if aruco1.getCenter().x < aruco2.getCenter().x:
            return [aruco1, aruco2]
        else:
            return [aruco2, aruco1]

    def checkIfFocused(self, eyes):
        """
        Verifies if the position of eyes is in the TaskElementPerFrame rectangle.
        """
        point = shapely.geometry.Point(eyes.x, eyes.y)
        polygon = shapely.geometry.Polygon(
            [[self.rect[0].x, self.rect[0].y], [self.rect[1].x, self.rect[1].y], [self.rect[2].x, self.rect[2].y],
             [self.rect[3].x, self.rect[3].y]])
        self.focused = point.within(polygon)
        # print("checkIfFocused=", self.focused)

    def getRectParallel(self):
        aRect = self.arucoLeft.rect
        bRect = self.arucoRight.rect

        sup_p1 = aRect[1]  # supporter Point, um die parallele Gerade zu zeichnen
        sup_p2 = bRect[0]

        qr_rect_top = self.task.qrRectPtsTopSorted  # Top-QR-Code reicht, um Hilfsgeraden zu erzeugen
        sup_l1_horizontal = (qr_rect_top[0], qr_rect_top[1])  # supporter Line, um die parallele Gerade zu zeichnen
        sup_l2_vertical = (qr_rect_top[0], qr_rect_top[2])
        p1 = parallel_line(sup_p1, sup_l1_horizontal, sup_p2, sup_l2_vertical)

        sup_p3 = aRect[1]
        sup_p4 = bRect[2]
        p2 = parallel_line(sup_p3, sup_l2_vertical, sup_p4, sup_l1_horizontal)

        p0 = aRect[1]
        p3 = bRect[2]

        return [p1, p0, p2, p3]


def parallel_line(pt_h, l_h, pt_v, l_v):
    # horizontal
    point_h = sympy.Point(pt_h.x, pt_h.y)

    p1_l_h = sympy.Point(l_h[0].x, l_h[0].y)
    p2_l_h = sympy.Point(l_h[1].x, l_h[1].y)
    line_h = sympy.Line(p1_l_h, p2_l_h)
    parallelLine_h = line_h.parallel_line(point_h)

    # vertical
    point_v = sympy.Point(pt_v.x, pt_v.y)

    p1_l_v = sympy.Point(l_v[0].x, l_v[0].y)
    p2_l_v = sympy.Point(l_v[1].x, l_v[1].y)
    line_v = sympy.Line(p1_l_v, p2_l_v)
    parallelLine_v = line_v.parallel_line(point_v)

    intersection = parallelLine_h.intersection(parallelLine_v)
    x = intersection[0][0]
    y = intersection[0][1]
    point = Point(x, y)
    return point