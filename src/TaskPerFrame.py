import numpy as np
import cv2
import shapely
import sympy

from collections import defaultdict
from Point import Point
from TaskElementPerFrame import TaskElementPerFrame


class TaskPerFrame:
    def __init__(self, id, qrRecPtsTop, qrRecPtsBot, timestamp):
        self.id = id
        self.timestamp = timestamp
        self.qrRecPtsTop = qrRecPtsTop
        self.qrRecPtsBot = qrRecPtsBot
        self.qrRectPtsTopSorted = self.getSortedPointsFromQrRect(self.qrRecPtsTop)
        self.qrRectPtsBotSorted = self.getSortedPointsFromQrRect(self.qrRecPtsBot)
        self.rect = self.createTaskRec()
        self.focused = False
        self.elements = []
        self.arucos = []

    def drawTaskElements(self, dframe, drawRectPts=True):
        for element in self.elements:
            element.rect = sortPoints(element.rect)
            topLeft, topRight, botLeft, botRight = element.rect

            # Draw Rectangle
            if element.focused:
                color = (0, 255, 0)  # grün
            else:
                color = (0, 0, 255)  # rot

            pts = np.array([[topLeft.x, topLeft.y], [topRight.x, topRight.y], [botRight.x, botRight.y],
                            [botLeft.x, botLeft.y]], np.int32)
            pts = pts.reshape((-1, 1, 2))
            cv2.polylines(dframe, [pts], isClosed=True, color=color, thickness=5)  # Rechteck malen

            # Draw Rectangle-Points
            if drawRectPts:
                color = (255, 150, 20)
                cv2.putText(dframe, "tr", (topRight.x - 15, topRight.y - 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9,
                            color, 2)
                cv2.putText(dframe, "br", (botRight.x - 15, botRight.y - 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9,
                            color, 2)
                cv2.putText(dframe, "bl", (botLeft.x - 15, botLeft.y - 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, color,
                            2)
                cv2.putText(dframe, "tl", (topLeft.x - 15, topLeft.y - 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, color,
                            2)

            # Draw Arucos
            arucos = [element.arucoLeft, element.arucoRight]
            for aruco in arucos:
                p0, p1, p2, p3 = aruco.rect

    def drawArucos(self, dframe):
        """
        Draws a rectangle and the decoded index of all aruco markers
        """
        for aruco in self.arucos:
            topLeft, topRight, botLeft, botRight = aruco.rect

            # Draw Rectangle
            pts = np.array([[topLeft.x, topLeft.y], [topRight.x, topRight.y], [botRight.x, botRight.y],
                            [botLeft.x, botLeft.y]], np.int32)
            pts = pts.reshape((-1, 1, 2))
            cv2.polylines(dframe, [pts], isClosed=True, color=(0, 0, 255), thickness=5)  # Rechteck malen

            # Draw Id
            cv2.putText(dframe, str(aruco.id), (botLeft.x + 15, botLeft.y + 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9,
                        (0, 255, 255), 2)

            # Draw Point
            x = aruco.getCenter().x
            y = aruco.getCenter().y
            cv2.circle(dframe, (x, y), 5, (255, 255, 0), -1)

    def createTaskElementPerFrames(self):
        """
        This Method creates TaskElements from this arucos-List after all ArUCo-Markers were recognized and have been
        filtered for this specific TaskPerFrame-Instanz
        """
        dict = defaultdict(list)
        elements = self.elements

        for i in self.arucos:
            dict[i.id].append(i)  # dict with elements of the list to fill by key

        for i in dict:  # go through every i-Key
            if len(dict[i]) == 2:
                # print("Index", i, "hat", len(dict[i]), "Element(e).")
                pair = [dict[i][0], dict[i][1]]
                element = TaskElementPerFrame(pair, self)
                elements.append(element)

    def checkIfFocused(self, eyes):
        """
        Schaut, ob die Position von Eyes in dem TaskPerFrame liegt.
        """
        point = shapely.geometry.Point(eyes.x, eyes.y)
        polygon = shapely.geometry.Polygon(
            [[self.rect[0].x, self.rect[0].y], [self.rect[1].x, self.rect[1].y], [self.rect[2].x, self.rect[2].y],
             [self.rect[3].x, self.rect[3].y]])
        self.focused = point.within(polygon)

    def createTaskRec(self):
        """
        Erzeugt aus der Position der QR-Codes die Viereck-Fläche des TaskPerFrame.
        """
        # print("Erzeuge Task-Area")
        line1 = (self.qrRectPtsTopSorted[0], self.qrRectPtsTopSorted[1])
        line2 = (self.qrRectPtsBotSorted[0], self.qrRectPtsBotSorted[2])
        p1 = line_intersection(line1, line2)

        line1 = (self.qrRectPtsTopSorted[1], self.qrRectPtsTopSorted[3])
        line2 = (self.qrRectPtsBotSorted[2], self.qrRectPtsBotSorted[3])
        p2 = line_intersection(line1, line2)

        point1 = self.qrRectPtsTopSorted[1]
        point2 = self.qrRectPtsBotSorted[2]
        p0 = Point(point1.x, point1.y)
        p3 = Point(point2.x, point2.y)
        return [p1, p0, p2, p3]

    def getSortedPointsFromQrRect(self, qrRecPts):
        """
        Die Punkteliste des erkannten QR-Code-Rechtecks ist immer in einer anderen Reihenfolge. Deshalb ist es notwendig
        sich die Codes zu sortieren. Diese Funktion schlüsselt die QR-Code-Punkte auf und bedient sich anschließend der
        sortPoints()-Funktion.
        """
        # Punkte für Task-Rechteck berechnen
        ## Punkte aus Barcode zwischenspeichern
        ptBarcode1 = Point(qrRecPts[0].x, qrRecPts[0].y)  # Rechteck-Punkte aus QR-Code extrahieren
        ptBarcode2 = Point(qrRecPts[1].x, qrRecPts[1].y)
        ptBarcode3 = Point(qrRecPts[2].x, qrRecPts[2].y)
        ptBarcode4 = Point(qrRecPts[3].x, qrRecPts[3].y)
        ptList = [ptBarcode1, ptBarcode2, ptBarcode3, ptBarcode4]
        ptListSorted = self.sortPoints(ptList)
        return ptListSorted

    def sortPoints(self, pointList):
        """
        Diese Funktion gibt dir aus vier Punkten (Typ: Point) eines Rechteckes eine sortierte Liste aus Points zurück.
        Es wird nach der Perspektive der Points sortiert: 0) oben links 1) oben rechts 2) unten links 3) unten rechts
        """
        pointListX = sorted(pointList, key=lambda point: point.x)  # Punkte nach x-Achse sortieren von links nach rechts

        topLeft, botLeft, topRight, botRight = 0, 0, 0, 0  # Punkte gruppieren: beide linken und beide rechten Punkte
        if pointListX[0].y < pointListX[
            1].y:  # Wir betrachten die beiden linken Punkte. Der obere Punkt soll in der Liste an erster Stelle kommen
            topLeft = pointListX[0]
            botLeft = pointListX[1]
        else:
            botLeft = pointListX[0]  # bot left
            topLeft = pointListX[1]  # top left

        if pointListX[2].y < pointListX[3].y:
            topRight = pointListX[2]
            botRight = pointListX[3]
        else:
            botRight = pointListX[2]
            topRight = pointListX[3]

        rectSorted = [topLeft, topRight, botLeft, botRight]
        return rectSorted

    def getPolygon(self):
        p0, p1, p2, p3 = self.rect[0], self.rect[1], self.rect[2], self.rect[3]
        polygon = np.array(
            [[p0.x, p0.y], [p1.x, p1.y], [p2.x, p2.y], [p3.x, p3.y]],
            np.int32)
        polygon = polygon.reshape((-1, 1, 2))
        return polygon


def sortPoints(pointList):
    """
    Diese Funktion gibt dir aus vier Punkten (Typ: Point) eines Rechteckes eine sortierte Liste aus Points zurück.
    Es wird nach der Perspektive der Points sortiert: 0) oben links 1) oben rechts 2) unten links 3) unten rechts
    """
    pointListX = sorted(pointList, key=lambda point: point.x)  # Punkte nach x-Achse sortieren von links nach rechts

    topLeft, botLeft, topRight, botRight = 0, 0, 0, 0  # Punkte gruppieren: beide linken und beide rechten Punkte
    if pointListX[0].y < pointListX[
        1].y:  # Wir betrachten die beiden linken Punkte. Der obere Punkt soll in der Liste an erster Stelle kommen
        topLeft = pointListX[0]
        botLeft = pointListX[1]
    else:
        botLeft = pointListX[0]  # bot left
        topLeft = pointListX[1]  # top left

    if pointListX[2].y < pointListX[3].y:
        topRight = pointListX[2]
        botRight = pointListX[3]
    else:
        botRight = pointListX[2]
        topRight = pointListX[3]

    rectSorted = [topLeft, topRight, botLeft, botRight]
    return rectSorted


def line_intersection(l1, l2):
    p1_l1 = sympy.Point(l1[0].x, l1[0].y)
    p2_l1 = sympy.Point(l1[1].x, l1[1].y)
    l1 = sympy.Line(p1_l1, p2_l1)

    p1_l2 = sympy.Point(l2[0].x, l2[0].y)
    p2_l2 = sympy.Point(l2[1].x, l2[1].y)
    l2 = sympy.Line(p1_l2, p2_l2)

    intersection = l1.intersection(l2)
    x = intersection[0][0]
    y = intersection[0][1]
    point = Point(x, y)
    return point