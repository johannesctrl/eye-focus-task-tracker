import json

from TimelineElementPerFrame import TimelineElementPerFrame


class TimelineElementsPerFrame:
    """
    Creates TimelineElementPerFrame for every TaskPerFrame and adds them to the list of this class. Sends
    TimelineElementsPerFrame to the Server.
    """

    def __init__(self, httpRequestPostUrl, out_q):
        self.timeline = []
        self.timelineChangedOnly = []
        self.lastTimelineElement = None
        self.httpRequestPostUrl = httpRequestPostUrl
        self.out_q = out_q

    def saveToJson(self, timelineElement):
        startTimestamp = timelineElement.startTimestamp
        te_json = timelineElement.toJson()

        folder_path = "outputs/"
        filename = str(startTimestamp) + ".json"
        file_path = folder_path + filename

        try:
            with open(file_path, "r") as jsonFile:
                data = json.load(jsonFile)
                data["inputs"].append(timelineElement.toDict())
                with open(file_path, "w") as jsonFile:
                    json.dump(data, jsonFile, indent=4, sort_keys=True)
        except IOError:
            print("File doesn't exist")
            data = {
                "inputs": []
            }
            data["inputs"].append(timelineElement.toDict())
            with open(file_path, "w") as jsonFile:
                json.dump(data, jsonFile, indent=4, sort_keys=True)
            return 0

    def importTasksPerFrame(self, tasksPerFrame, userId, startTimestamp, timestamp):
        """
        In this method all focused TasksPerFrame are getting analysed to create one TimelineElementPerFrame for
        the specific TaskPerFrame which is focused (because it's only possible that one Task is focused) and add those
        to this class's timeline-list. This method also calls the method "importToChangedOnly".
        """
        timelineElement = TimelineElementPerFrame()
        timelineElement.userId = userId
        timelineElement.startTimestamp = startTimestamp
        timelineElement.timestamp = timestamp

        for tpf in tasksPerFrame.taskList:
            if tpf.focused:
                timelineElement.taskId = tpf.id
                for e in tpf.elements:
                    if e.focused:
                        timelineElement.elementId = int(e.id)

        self.timeline.append(timelineElement)
        self.importToChangedOnly(timelineElement)
        self.lastTimelineElement = timelineElement

    def importToChangedOnly(self, timelineElementPerFrame):
        """
        In this method every created TimelineElementPerFrame is getting controlled about if its attributes have the same
        values than the the ones of the previous TimelineElementPerFrame-Instanz. If the values don't match it means
        that something in the task-recognition or the focused has changed. Not all but all different
        TimelineElementPerFrame instances should be sent to the L2P-Service. This reduces the count of transmitted data.
        """
        url = self.httpRequestPostUrl
        data = timelineElementPerFrame.toJson()

        if len(self.timelineChangedOnly) == 0:  # for the very first TimelineElementPerFrame
            self.timelineChangedOnly.append(timelineElementPerFrame)
            print("append first element")
            self.out_q.put((url, data))
            # sendToL2P(url, data)
            # self.saveToJson(timelineElement)
        else:
            taskId = str(timelineElementPerFrame.taskId)
            elementId = str(timelineElementPerFrame.elementId)
            lastTaskId = str(self.lastTimelineElement.taskId)
            lastElementId = str(self.lastTimelineElement.elementId)
            if not (taskId == lastTaskId and elementId == lastElementId):
                print("changed focus")
                self.timelineChangedOnly.append(timelineElementPerFrame)
                self.out_q.put((url, data))
                # sendToL2P(url, data)
                # self.saveToJson(timelineElement)
