import cv2
import cv2.aruco as aruco
import numpy as np
import shapely
import sympy

from TaskPerFrame import TaskPerFrame
from Point import Point
from Aruco import Aruco


class TasksPerFrame:
    """
    Diese Klasse analysiert eine Liste von QR-Codes und gibt am Ende eine Liste von Tasks aus, die es dann später
    gilt mit schon gefundenen Tasks abzugleichen, was in dieser Klasse selber nicht passieren soll, denn eine Instanz
    dieser Klasse lebt nur einen Frame lang.
    """

    def __init__(self, frameNumber):
        self.taskList = []
        self.arucos = []
        self.frameNumber = frameNumber

    def findPairs(self, l):
        l.sort()
        l_tupel = []  # Der QR-Code aufgeschlüsselt in ID und top/bot
        l_pairs = []  # Gefundene Paare
        for element in l:
            l_tupel.append(element.split())
        for i in range(len(l_tupel)):
            for j in range(i + 1, len(l_tupel)):
                if l_tupel[i][0] == l_tupel[j][0]:
                    l_pairs.append([l_tupel[i], l_tupel[j]])
        return l_pairs

    def drawTaskElements_ForAllTasks(self, dframe):
        for task in self.taskList:
            color = (0, 0, 0)
            content = str(len(task.elements)) + " Elements detected"
            task.drawTaskElements(dframe)
            x = task.rect[0].x
            y = task.rect[0].y
            cv2.putText(dframe, content, (x + 30, y - 30), 1, 1.4, color, 1)
            # cv2.putText(dframe, "br", (botRight.x - 15, botRight.y - 15), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, color, 2)

    def drawArucos_ForAllTasks(self, dframe):
        for task in self.taskList:
            task.drawArucos(dframe)

    def drawRecsQrCodes_ForAllTasks(self, dframe):
        """ Diese Methode malt für alle Tasks des Frames ein Rechteck um die QR-Codes """
        color = (255, 0, 255)
        for task in self.taskList:
            qrList = [task.qrRecPtsTop, task.qrRecPtsBot]
            for qrRecPts in qrList:
                pts = np.array([qrRecPts], np.int32)
                pts = pts.reshape((-1, 1, 2))
                cv2.polylines(dframe, [pts], True, color, 5)

    def drawID_ForAllTasks(self, dframe):
        color = (0, 0, 255)
        for task in self.taskList:
            content = task.id
            point = task.rect[1]
            cv2.putText(dframe, content, (int(point.x + 30), int(point.y - 30)), 1, 1.4, color, 1)

    def drawRect_ForAllTasks(self, dframe):
        color = (0, 0, 0)
        for task in self.taskList:
            if task.focused:
                color = (0, 255, 0)  # grün
            else:
                color = (0, 0, 255)  # rot
            cv2.polylines(dframe, [task.getPolygon()], True, color, 5)  # Rechteck malen

    def drawRectPoint_ForAllTasks(self, dframe):
        color = (123, 255, 132)
        for task in self.taskList:
            pts = task.getPolygon()
            for i, point in enumerate(pts):  # iteriert durch alle 4 Punkte des QR-Codes
                px = point[0][0]
                py = point[0][1]
                pointText = str(i) + " : " + str(px) + " | " + str(py)
                cv2.putText(dframe, pointText, (px + 30, py + 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.9, color, 1)

    def checkIfFocused_ForAllTasks(self, eyes):
        for task in self.taskList:
            task.checkIfFocused(eyes)
            for element in task.elements:
                # print("Check Elements if focused...")
                element.checkIfFocused(eyes)

    def decodeFrame(self, decodedFrame, timestamp):
        # print("decode...")
        # Finde Pärchen
        ## Extrahiere String aus QR-Code
        listQRStrings = []
        for qrCode in decodedFrame:
            decodedText = qrCode.data.decode("UTF-8")
            listQRStrings.append(decodedText)

        ## Finde String-Pärchen
        listQRStringsPaired = self.findPairs(listQRStrings)
        # print(len(listQRStringsPaired), "pair(s) found.")

        ## Gehe durch decodedFrame aber nur durch Pärchen
        for pairElement in listQRStringsPaired:  # durch alle Paar-Element-Strings iterieren
            # Temporary-Task - Attributes
            id_tta = ""
            qrRecPtsTop_tta = ""
            qrRecPtsBot_tta = ""

            for qrCode in decodedFrame:
                decodedText = qrCode.data.decode("UTF-8")
                decodedPts = qrCode.polygon
                # print("Dekodierter Text =", decodedText)
                # print("Dekodierte Punkte =", decodedPts)
                for singleElement in pairElement:  # for Bot und Top
                    singleElementFullString = " ".join([str(singleElement[0]), str(singleElement[1])])
                    # print(singleElementFullString)
                    if singleElementFullString == decodedText:  # abgleichen aller gefundener QR-Codes mit Paar-Element-String-Liste
                        id_tta = singleElementFullString[:-4]  # lösche das bot / top Suffix
                        # print("id_tta", id_tta)
                        posType = singleElement[1]  # bot oder top
                        # print("PosType:", posType)
                        if posType == "top":
                            qrRecPtsTop_tta = decodedPts  # Punkte des einzelnen QR-Code-Rechtecks
                            # print("Points Top:", qrRecPtsTop_tta)
                        elif posType == "bot":
                            qrRecPtsBot_tta = decodedPts
                            # print("POints Bot", qrRecPtsBot_tta)

            taskPerFrame = TaskPerFrame(id_tta, qrRecPtsTop_tta, qrRecPtsBot_tta, timestamp)
            # taskPerFrame.createTaskRec()
            self.taskList.append(taskPerFrame)

    def decodeArucoMarkers(self, frame, dframe, markerSize=4, totalMarkers=1000):
        """
        Diese Methode dekodiert alle Aruco-Marker innerhalb eines Frames und legt entsprechend Aruco-Instanzen an. Dabei
        sind die Aruco-Marker noch nicht danach gefiltert, ob sie wirklich innerhalb einer Task liegen.
        """
        # print("Decode Aruco Markers")

        key = getattr(aruco, f'DICT_{markerSize}X{markerSize}_{totalMarkers}')
        arucoDict = aruco.Dictionary_get(key)
        arucoParam = aruco.DetectorParameters_create()
        corners, ids, rejectedMarkers = aruco.detectMarkers(frame, arucoDict, parameters=arucoParam)
        cv2.putText(dframe, "MarkerCount = " + str(len(corners)), (int(30), int(60)), 1, 1.4, (0, 0, 255), 1)

        if len(corners) > 0:
            # print("Corner:")
            counter = 0
            for i, corner in enumerate(corners):
                counter += 1
                topLeft = Point(int(corners[i][0][0][0]), int(corners[i][0][0][1]))  # topLeft
                topRight = Point(int(corners[i][0][1][0]), int(corners[i][0][1][1]))  # topRight
                botRight = Point(int(corners[i][0][2][0]), int(corners[i][0][2][1]))  # botRight
                botLeft = Point(int(corners[i][0][3][0]), int(corners[i][0][3][1]))  # botLeft
                rectangle = [topLeft, topRight, botLeft, botRight]

                id = ids[i][0]
                # print("id=" + str(id))
                arucoMarker = Aruco(id, rectangle)
                self.arucos.append(arucoMarker)

    def assignArucosToTasks(self):
        """
        Nachdem die Aruco-Marker erkannt wurden wird mit dieser Methode geschaut, ob sich ein Marker innerhalb einer Task befindet.
        """
        for task in self.taskList:
            for aruco in self.arucos:
                aruco_center = aruco.getCenter()
                point = shapely.geometry.Point(aruco_center.x, aruco_center.y)
                p1 = task.rect[0]
                p2 = task.rect[1]
                p3 = task.rect[2]
                p4 = task.rect[3]
                polygon = shapely.geometry.Polygon([[p1.x, p1.y], [p2.x, p2.y], [p3.x, p3.y], [p4.x, p4.y]])
                arucoIsInTask = point.within(polygon)
                if arucoIsInTask:
                    # print("element " + str(element.id) + " is in task " + task.id)
                    task.arucos.append(aruco)

    def createTaskElementPerFrames_ForAllTasks(self):
        for task in self.taskList:
            task.createTaskElementPerFrames()