import os

from ListboxElement import ListboxElement
from SessionFolder import SessionFolder
from PathCollection import PathCollection


class MainFolder:
    """
    main <--
        session_01
            000_recording
                world.mp4 (for narrow angle lens)
                info.player.json (for narrow angle lens)
                exports
                    000_export
                        gaze_positions.csv (for narrow angle lens)
                        iMotions
                            gaze.tlv (for wide angle lens)
                            iMotions.info (for wide angle lens)
                            scene.mp4 (for wide angle lens)
                    001_export
            001_recording
            002_recording
        session_02
            000_recording
            001_recording
    """
    def __init__(self, path):
        self.path = path
        self.session_folder_list = self.create_session_folders()
        # self.print_recording_folders()

    def create_session_folders(self):
        """ Creates instances of SessionFolder by every found folder. """
        session_folder_list = []
        for folderName in os.listdir(self.path):
            path = self.path + folderName + "/"
            session_folder = SessionFolder(path, self)
            session_folder_list.append(session_folder)
        return session_folder_list

    def create_path_collection_list(self):
        path_collection_list = []
        for session_folder in self.session_folder_list:
            for recording_folder in session_folder.recording_folder_list:
                print(session_folder.name + "/" + recording_folder.name)
                name = session_folder.name + "/" + recording_folder.name
                exportsPath = recording_folder.path
                videoPath = recording_folder.get_iMotions_video_path()
                gazeTLVPath = recording_folder.get_iMotions_tlv_path()
                path_collection = PathCollection(name, exportsPath, videoPath, gazeTLVPath, recording_folder)
                path_collection_list.append(path_collection)
        return path_collection_list

    def create_recording_collection(self):
        recording_collection = []
        for session_folder in self.session_folder_list:
            for recording_folder in session_folder.recording_folder_list:
                print(session_folder.name + "/" + recording_folder.name)
                name = session_folder.name + "/" + recording_folder.name
                recording_collection = ListboxElement(name, recording_folder)
                recording_collection.append(recording_collection)
        return recording_collection

    def print_recording_folders(self):
        for session_folder in self.session_folder_list:
            print(" " + session_folder.name)
            for recording_folder in session_folder.recording_folder_list:
                print(" -" + recording_folder.name)
