from queue import Queue
from threading import Thread


def run_in_threads(producer, consumer):
    """
    This method is called to create a producer and a consumer thread. The consumer thread
    is for sending JSON-Statements to the Las2Peer-Service. The producer function is e.g.
    openLiveWebcam, etc. This method exists to call all functions that analyse the video
    and gaze material as producer functions.
    """

    q = Queue()
    t1 = Thread(target=producer, args=(q,))
    t2 = Thread(target=consumer, args=(q,))
    t1.start()
    t2.start()
    t1.join()
    t2.join()
