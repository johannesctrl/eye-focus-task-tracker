from tkinter import END

import numpy as np
import cv2
import shapely.geometry
import zmq
from msgpack import packb, unpackb
from pyzbar.pyzbar import decode
from pyzbar.pyzbar import ZBarSymbol
import xml.etree.cElementTree as ET
import csv
import matplotlib.pyplot as plt
import cv2.aruco as aruco
from collections import defaultdict
import sympy
import time
import os
import datetime
from datetime import datetime
import json
import requests
import tkinter as tk
import threading
from queue import Queue
from threading import Thread


def sendToL2P(url, data):
    # print("sendToL2P")
    # print("url", url)
    # print("data", data)
    # url = "https://las2peer.tech4comp.dbis.rwth-aachen.de/uitProxy/postEyeTracking"
    r = requests.post(url=url, data=data)
    # print(r)
    # r = requests.post(url=url, data=data, timeout=5.0)
    if r.ok:
        print("Posting Data to L2P was successful.")
    else:
        print("Posting Data to L2P wasn't successful.")
    # print("END sendtoL2P")






##############################################
# Funktionen
##############################################

def getPolygon(rect):
    """
    Erzeugt aus vier Punkt-Objekten ein Polygon für cv2, um es zu zeichnen
    """
    p0 = rect[0]
    p1 = rect[1]
    p2 = rect[2]
    p3 = rect[3]
    polygon = np.array([[p0.x, p0.y], [p1.x, p1.y], [p2.x, p2.y], [p3.x, p3.y]], np.int32)
    polygon = polygon.reshape((-1, 1, 2))
    return polygon


def sortPoints(pointList):
    """
    Diese Funktion gibt dir aus vier Punkten (Typ: Point) eines Rechteckes eine sortierte Liste aus Points zurück.
    Es wird nach der Perspektive der Points sortiert: 0) oben links 1) oben rechts 2) unten links 3) unten rechts
    """
    pointListX = sorted(pointList, key=lambda point: point.x)  # Punkte nach x-Achse sortieren von links nach rechts

    topLeft, botLeft, topRight, botRight = 0, 0, 0, 0  # Punkte gruppieren: beide linken und beide rechten Punkte
    if pointListX[0].y < pointListX[
        1].y:  # Wir betrachten die beiden linken Punkte. Der obere Punkt soll in der Liste an erster Stelle kommen
        topLeft = pointListX[0]
        botLeft = pointListX[1]
    else:
        botLeft = pointListX[0]  # bot left
        topLeft = pointListX[1]  # top left

    if pointListX[2].y < pointListX[3].y:
        topRight = pointListX[2]
        botRight = pointListX[3]
    else:
        botRight = pointListX[2]
        topRight = pointListX[3]

    rectSorted = [topLeft, topRight, botLeft, botRight]
    return rectSorted


def line_intersection(l1, l2):
    p1_l1 = sympy.Point(l1[0].x, l1[0].y)
    p2_l1 = sympy.Point(l1[1].x, l1[1].y)
    l1 = sympy.Line(p1_l1, p2_l1)

    p1_l2 = sympy.Point(l2[0].x, l2[0].y)
    p2_l2 = sympy.Point(l2[1].x, l2[1].y)
    l2 = sympy.Line(p1_l2, p2_l2)

    intersection = l1.intersection(l2)
    x = intersection[0][0]
    y = intersection[0][1]
    point = Point(x, y)
    return point


def parallel_line(pt_h, l_h, pt_v, l_v):
    # horizontal
    point_h = sympy.Point(pt_h.x, pt_h.y)

    p1_l_h = sympy.Point(l_h[0].x, l_h[0].y)
    p2_l_h = sympy.Point(l_h[1].x, l_h[1].y)
    line_h = sympy.Line(p1_l_h, p2_l_h)
    parallelLine_h = line_h.parallel_line(point_h)

    # vertical
    point_v = sympy.Point(pt_v.x, pt_v.y)

    p1_l_v = sympy.Point(l_v[0].x, l_v[0].y)
    p2_l_v = sympy.Point(l_v[1].x, l_v[1].y)
    line_v = sympy.Line(p1_l_v, p2_l_v)
    parallelLine_v = line_v.parallel_line(point_v)

    intersection = parallelLine_h.intersection(parallelLine_v)
    x = intersection[0][0]
    y = intersection[0][1]
    point = Point(x, y)
    return point


def parallel_from_point(pt, l, l2):
    point = sympy.Point(pt.x, pt.y)

    p1_l = sympy.Point(l[0].x, l[0].y)
    p2_l = sympy.Point(l[1].x, l[1].y)
    line = sympy.Line(p1_l, p2_l)

    parallelLine = line.parallel_line(point)

    p1_l2 = sympy.Point(l2[0].x, l2[0].y)
    p2_l2 = sympy.Point(l2[1].x, l2[1].y)
    l2 = sympy.Line(p1_l2, p2_l2)

    intersection = parallelLine.intersection(l2)
    x = intersection[0][0]
    y = intersection[0][1]
    point = Point(x, y)
    return point


def scaleImage(img, scale_percent=60):
    """ Skaliert ein Bild um x Prozent. """
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    return img


def createXMLFromTaskRecords(taskRecords):
    session = ET.Element("session")
    person = ET.SubElement(session, "person")
    mail = ET.SubElement(person, "mail").text = "surname.lastname@stud.htwk-leipzig.de"
    records = ET.SubElement(session, "records")
    tasks = ET.SubElement(records, "tasks")
    for tr in taskRecords.list:
        task = ET.SubElement(tasks, "task")
        id = ET.SubElement(task, "id").text = tr.id
        framecounters = ET.SubElement(task, "framecounters")
        visable = ET.SubElement(framecounters, "visable").text = str(tr.frameCounter)
        focused = ET.SubElement(framecounters, "focused").text = str(tr.focusedFrameCounter)
        unfocused = ET.SubElement(framecounters, "unfocused").text = str(tr.unfocusedFrameCounter)

    tree = ET.ElementTree(session)
    folder = "outputs"
    try:
        tree.write(folder + "/session.xml")
    except ValueError:
        print("Folder", folder, "could not be found.")


def drawControls(dframe):
    wOffset = 20
    h, w, _ = dframe.shape
    # print("width="+ str(w), "height=" + str(h))
    color = (255, 255, 0)
    quit_content = "Press q to quit"
    xml_content = "Press x to create xml-file"
    changeCamera_content = "Press numbers from 0 to 4 to change camera input"
    cv2.putText(dframe, changeCamera_content, (wOffset, h - 90), 1, 1.9, color, 1)
    cv2.putText(dframe, quit_content, (wOffset, h - 60), 1, 1.9, color, 1)
    cv2.putText(dframe, xml_content, (wOffset, h - 30), 1, 1.9, color, 1)



def importGazePosition(path):
    x = []
    y = []
    world_index = -1
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader)
        for line in csv_reader:
            print(line)
            if (world_index == int(line[1])):
                next(csv_reader)
                print(world_index)
            else:
                world_index += 1
                world_index = int(line[1])
                norm_pos_x = float(line[3])
                norm_pos_y = float(line[4])
                print(norm_pos_x, norm_pos_y)
                x.append(norm_pos_x)
                y.append(norm_pos_y)
    return x, y


class PupilDataFishEyePathCollection:
    def __init__(self, name, exportsPath, videoPath, gazeTLVPath, pupilRecordingFolder):
        self.name = name
        self.exportsPath = exportsPath
        self.videoPath = videoPath
        self.gazeTLVPath = gazeTLVPath
        self.pupilRecordingFolder = pupilRecordingFolder


def openPupilDataFishEye(exportsPath, videoPath, gazeTLVPath, pupilRecordingFolder):
    # capture = cv2.VideoCapture(videoPath)
    capture = cv2.VideoCapture(pupilRecordingFolder.get_iMotions_video_path())

    res_x = int(capture.get(3))
    res_y = int(capture.get(4))
    print("Video resolution is: " + str(res_x) + "x" + str(res_y))

    eyes = Eyes()
    gazeTLVFile = GazeTLVFile(gazeTLVPath)
    xList = gazeTLVFile.get_column_by_name("Gaze2dX")
    yList = gazeTLVFile.get_column_by_name("Gaze2dY")
    timestampList = gazeTLVFile.get_column_by_name("GazeTimeStamp")
    startTimestamp_ms = pupilRecordingFolder.get_iMotions_info_dict()["Start Time (System)"]

    taskRecords = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
    frameNumber = 0
    fps = 0
    prev_frame_time = 0

    frameTimeline = []

    while capture.isOpened:  # existiert die Datei, bzw. der Livestream (noch)
        success, frame = capture.read()
        if not success:
            print("Video has ended.")
            capture.release()
            cv2.destroyWindow("frame")
            break
        dframe = frame.copy()

        frameNumber += 1
        frameTimeline.append(frameNumber)

        windowsize = 80
        xPos = xList[frameNumber]
        yPos = yList[frameNumber]
        # print(xPos, yPos)
        eyes.set_x(xPos)
        eyes.set_y(yPos)

        cv2.circle(dframe, (int(eyes.x), int(eyes.y)), 10, (255, 0, 0), -1)  # visualisiere Eye-Tracking

        # QR-Scanner
        decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
        taskListPerFrame = TasksPerFrame(frameNumber)
        taskListPerFrame.decodeFrame(decodedFrame,
                                     pupilRecordingFolder.convert_pupil_time_to_system_time(timestampList[frameNumber]))

        # Sind Tasks fokussiert?
        taskListPerFrame.checkIfFocused_ForAllTasks(eyes)

        # Speichere Ergebnisse
        taskRecords.importTasksPerFrame(taskListPerFrame, frameNumber)
        taskRecords.drawTasks(dframe)

        # Zeichne
        taskListPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
        taskListPerFrame.drawRect_ForAllTasks(dframe)
        taskListPerFrame.drawRectPoint_ForAllTasks(dframe)
        taskListPerFrame.drawID_ForAllTasks(dframe)
        cv2.putText(dframe, "fps=" + str(fps), (res_x - 500, res_y - 10), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.imshow("frame", scaleImage(dframe, windowsize))

        # Controls
        pressedKey = cv2.waitKey(1) & 0xFF
        if pressedKey == ord('q'):  # schließen
            cv2.destroyWindow("frame")
            capture.release()
            break

        if pressedKey == ord('x'):  # createXML
            createXMLFromTaskRecords(taskRecords)

        if pressedKey == ord('s'):  # createXML
            taskRecords.drawTimeline(frameNumber)

        # FPS
        new_frame_time = time.time()
        fps = 1 / (new_frame_time - prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)


def openPupilData(pathFolderRecording="resources/recording_24_03/"):
    exportsPath = pathFolderRecording + "exports/000/"
    capture = cv2.VideoCapture(exportsPath + "world.mp4")
    # capture.set(3, 1280)  # ID bei set für die Breite ist 3
    # capture.set(4, 720)

    eyes = Eyes()  # Die Augen sind in der Mitte der Bild-Aufnahme
    # xList, yList = importGazePosition(exportsPath)
    xList, yList = importGazePosition(
        "D:/Git-Clones/digitalMentor/ressources/recording_24_03/exports/000/gaze_positions.csv")

    taskRecords = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
    frameNumber = 0
    fps = 0
    prev_frame_time = 0

    frameTimeline = []

    while capture.isOpened:
        success, frame = capture.read()
        dframe = frame.copy()

        frameNumber += 1
        frameTimeline.append(frameNumber)

        timestamp = int(datetime.now().strftime("%s%f")) / 1000
        print(timestamp)

        windowsize = 100
        xPos = xList[frameNumber] * 1280 / (windowsize / 100)
        yPos = 720 - yList[frameNumber] * 720 / (windowsize / 100)
        print(xPos, yPos)
        eyes.set_x(xPos)
        eyes.set_y(yPos)

        cv2.circle(dframe, (int(eyes.x), int(eyes.y)), 10, (255, 0, 0), -1)  # visualisiere Eye-Tracking

        # QR-Scanner
        decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
        taskListPerFrame = TasksPerFrame(frameNumber)
        taskListPerFrame.decodeFrame(decodedFrame, timestamp)

        # Sind Tasks fokussiert?
        taskListPerFrame.checkIfFocused_ForAllTasks(eyes)

        # Speichere Ergebnisse
        taskRecords.importTasksPerFrame(taskListPerFrame, frameNumber)
        taskRecords.drawTasks(dframe)

        # Zeichne
        taskListPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
        taskListPerFrame.drawRect_ForAllTasks(dframe)
        taskListPerFrame.drawRectPoint_ForAllTasks(dframe)
        taskListPerFrame.drawID_ForAllTasks(dframe)
        cv2.putText(dframe, "fps=" + str(fps), (1920 - 500, 1080 - 10), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.imshow("frame", scaleImage(dframe, windowsize))

        # Controlls
        pressedKey = cv2.waitKey(1) & 0xFF
        if pressedKey == ord('q'):  # schließen
            cv2.destroyWindow("frame")
            capture.release()
            break

        if pressedKey == ord('x'):  # createXML
            createXMLFromTaskRecords(taskRecords)

        if pressedKey == ord('s'):  # createXML
            taskRecords.drawTimeline(frameNumber)

        # FPS
        new_frame_time = time.time()
        fps = 1 / (new_frame_time - prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)
    capture.release()


_sentinel = object()


def runEyeTracking(producerFunction, *args):
    """
    This method is called to create a producer and a consumer thread. The consumer thread
    is for sending JSON-Statements to the Las2Peer-Service. The producer function is e.g.
    openLiveWebcam, etc. This method exists to call all functions that analyse the video
    and gaze material as producer functions.
    """
    q = Queue()
    t1 = Thread(target=producerFunction, args=args + (q,))
    t2 = Thread(target=consumer, args=(q,))
    t1.start()
    t2.start()
    t1.join()
    t2.join()


def consumer(in_q):
    while True:
        data = in_q.get()
        json_data = None
        url = None
        if data is _sentinel:
            in_q.put(_sentinel)
            break
        else:
            url = data[0]
            json_data = data[1]
            print(json_data)
        sendToL2P(url, json_data)
        in_q.task_done()


def openLivePupilCore(windowSize, userId, httpRequestPostUrl, out_q):
    # Methods
    def notify(notification):
        """Sends ``notification`` to Pupil Remote"""
        topic = "notify." + notification["subject"]
        payload = packb(notification, use_bin_type=True)
        req.send_string(topic, flags=zmq.SNDMORE)
        req.send(payload)
        return req.recv_string()



    # port setup
    addr = "127.0.0.1"  # open a req port to talk to pupil; remote ip or localhost
    req_port = "50020"  # same as in the pupil remote gui
    context = zmq.Context()
    req = context.socket(zmq.REQ)
    req.connect("tcp://{}:{}".format(addr, req_port))
    req.send_string("SUB_PORT")  # ask for the sub port
    sub_port = req.recv_string()
    sub = context.socket(zmq.SUB)  # open a sub port to listen to pupil
    sub.connect("tcp://{}:{}".format(addr, sub_port))
    sub.setsockopt_string(zmq.SUBSCRIBE, "frame.")
    sub.setsockopt_string(zmq.SUBSCRIBE, "gaze.")

    def recv_from_sub():
        """Recv a message with topic, payload.
        Topic is a utf-8 encoded string. Returned as unicode object.
        Payload is a msgpack serialized dict. Returned as a python dict.
        Any addional message frames will be added as a list
        in the payload dict with key: '__raw_data__' .
        """
        topic = sub.recv_string()
        payload = unpackb(sub.recv(), raw=False)
        extra_frames = []
        while sub.get(zmq.RCVMORE):
            extra_frames.append(sub.recv())
        if extra_frames:
            payload["__raw_data__"] = extra_frames
        return topic, payload

    def has_new_data_available():
        # Returns True as long subscription socket has received data queued for processing
        return sub.get(zmq.EVENTS) & zmq.POLLIN

    fisheye_properties = {
        "(1920, 1080)": {
            "dist_coefs": [
                [
                    -0.13648546769272826,
                    -0.0033787366635030644,
                    -0.002343859061730869,
                    0.001926274947199097,
                ]
            ],
            "camera_matrix": [
                [793.8052697386686, 0.0, 953.2237035923064],
                [0.0, 792.3104221704713, 572.5036513432223],
                [0.0, 0.0, 1.0],
            ],
            "cam_type": "fisheye"
        }
    }
    D = fisheye_properties["(1920, 1080)"]["dist_coefs"]
    D = np.array(D)
    K = fisheye_properties["(1920, 1080)"]["camera_matrix"]
    # print(K, type(K))
    K = np.array(K).reshape(3, 3)

    def undistort(img):
        """
        Undistortes an image based on the camera model.
        :param img: Distorted input image
        :return: Undistorted image
        """
        R = np.eye(3)

        map1, map2 = cv2.fisheye.initUndistortRectifyMap(
            np.array(K),
            np.array(D),
            R,
            np.array(K),
            resolution,
            cv2.CV_16SC2,
        )

        undistorted_img = cv2.remap(
            img,
            map1,
            map2,
            interpolation=cv2.INTER_LINEAR,
            borderMode=cv2.BORDER_CONSTANT,
        )

        return undistorted_img

    def projectPoints(object_points, rvec=None, tvec=None, use_distortion=True):
        """
        Projects a set of points onto the camera plane as defined by the camera model.
        :param object_points: Set of 3D world points
        :param rvec: Set of vectors describing the rotation of the camera when recording
            the corresponding object point
        :param tvec: Set of vectors describing the translation of the camera when
            recording the corresponding object point
        :return: Projected 2D points
        """
        skew = 0

        input_dim = object_points.ndim

        object_points = object_points.reshape((1, -1, 3))

        if rvec is None:
            rvec = np.zeros(3).reshape(1, 1, 3)
        else:
            rvec = np.array(rvec).reshape(1, 1, 3)

        if tvec is None:
            tvec = np.zeros(3).reshape(1, 1, 3)
        else:
            tvec = np.array(tvec).reshape(1, 1, 3)

        if use_distortion:
            _D = D
        else:
            _D = np.asarray([[1.0 / 3.0, 2.0 / 15.0, 17.0 / 315.0, 62.0 / 2835.0]])

        image_points, jacobian = cv2.fisheye.projectPoints(
            object_points, rvec, tvec, K, _D, alpha=skew
        )

        if input_dim == 2:
            image_points.shape = (-1, 2)
        elif input_dim == 3:
            image_points.shape = (-1, 1, 2)
        return image_points

    def unprojectPoints(pts_2d, use_distortion=True, normalize=False):
        """
        Undistorts points according to the camera model. cv2.fisheye.undistortPoints
        does *NOT* perform the same unprojection step the original cv2.unprojectPoints
        does. Thus we implement this function ourselves.
        :param pts_2d, shape: Nx2
        :return: Array of unprojected 3d points, shape: Nx3
        """

        pts_2d = np.array(pts_2d, dtype=np.float32)

        # Delete any posibly wrong 3rd dimension
        if pts_2d.ndim == 1 or pts_2d.ndim == 3:
            pts_2d = pts_2d.reshape((-1, 2))

        eps = np.finfo(np.float32).eps
        # print("hier:", K, type(K))
        f = np.array((K[0, 0], K[1, 1])).reshape(1, 2)
        c = np.array((K[0, 2], K[1, 2])).reshape(1, 2)
        if use_distortion:
            k = D.ravel().astype(np.float32)
        else:
            k = np.asarray(
                [1.0 / 3.0, 2.0 / 15.0, 17.0 / 315.0, 62.0 / 2835.0], dtype=np.float32
            )

        pi = pts_2d.astype(np.float32)
        pw = (pi - c) / f

        theta_d = np.linalg.norm(pw, ord=2, axis=1)
        theta = theta_d
        for j in range(10):
            theta2 = theta ** 2
            theta4 = theta2 ** 2
            theta6 = theta4 * theta2
            theta8 = theta6 * theta2
            theta = theta_d / (
                    1 + k[0] * theta2 + k[1] * theta4 + k[2] * theta6 + k[3] * theta8
            )

        scale = np.tan(theta) / (theta_d + eps)

        pts_2d_undist = pw * scale.reshape(-1, 1)

        pts_3d = cv2.convertPointsToHomogeneous(pts_2d_undist)
        pts_3d.shape = -1, 3

        if normalize:
            pts_3d /= np.linalg.norm(pts_3d, axis=1)[:, np.newaxis]
        return pts_3d

    def denormalize(pos, size, flip_y=False):
        """
        denormalize
        """
        width, height = size
        x = pos[0]
        y = pos[1]
        x *= width
        if flip_y:
            y = 1 - y
        y *= height
        return x, y

    def undist2d2pixel(global_norm_pos_x, global_norm_pos_y, resolution):
        pixel_pos = denormalize(
            (global_norm_pos_x, global_norm_pos_y), resolution, flip_y=True
        )
        undistorted3d = unprojectPoints(pixel_pos)
        undistorted2d = projectPoints(
            undistorted3d, use_distortion=False
        )
        width, height = resolution
        x, y = (int(undistorted2d[0][0]), int(undistorted2d[0][1]))
        if x > width:
            x = width
        if y > height:
            y = height
        if x < 0:
            x = 0
        if y < 0:
            y = 0
        return (x, y)


    _sentinel = object()
    global_norm_pos_x = 0
    global_norm_pos_y = 0
    frame = None
    res_x = 0
    res_y = 0
    resolution = (res_x, res_y)
    records = []
    start_time = time.time()
    FRAME_FORMAT = "bgr"
    notify({"subject": "frame_publishing.set_format",
            "format": FRAME_FORMAT})  # Set the frame format via the Network API plugin

    eyes = Eyes()  # Die Augen sind per default in der Mitte der Bild-Aufnahme
    taskRecords = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
    timeline = TimelineElementsPerFrame(httpRequestPostUrl=httpRequestPostUrl, out_q=out_q)

    # timestamps
    startTimestamp_raw = time.time()  # in seconds but float
    startTimestamp_ms = int(startTimestamp_raw * 1000)

    # frames
    frameNumber = 0
    fps = 0
    prev_frame_time = 0
    frameTimeline = []

    try:
        while True:
            while has_new_data_available():

                topic, msg = recv_from_sub()

                if topic.startswith("gaze."):  # get gaze

                    global_norm_pos_x, global_norm_pos_y = msg['norm_pos']

                if topic.startswith("frame.") and msg["format"] != FRAME_FORMAT:
                    print(
                        f"different frame format ({msg['format']}); "
                        f"skipping frame from {topic}"
                    )
                    continue

                if topic == "frame.world":  # get video
                    frame = np.frombuffer(
                        msg["__raw_data__"][0],
                        dtype=np.uint8).reshape(msg["height"], msg["width"], 3)
                    res_x = msg['width']
                    res_y = msg['height']
                    resolution = (res_x, res_y)
            if frame is not None and global_norm_pos_x is not 0:
                img_undist = undistort(frame)
                frame = img_undist
                dframe = frame.copy()

                gaze_pixel = undist2d2pixel(global_norm_pos_x, global_norm_pos_y, resolution)

                # timestamps
                cTimestamp_raw = time.time()  # in seconds but float
                cTimestamp_ms = int(cTimestamp_raw * 1000)

                # frames
                frameNumber += 1
                frameTimeline.append(frameNumber)

                # qr code scanner
                decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
                tasksPerFrame = TasksPerFrame(frameNumber)
                tasksPerFrame.decodeFrame(decodedFrame, cTimestamp_ms)

                # AruCo scanner
                tasksPerFrame.decodeArucoMarkers(frame, dframe)
                tasksPerFrame.assignArucosToTasks()
                tasksPerFrame.createTaskElementPerFrames_ForAllTasks()

                # focused?
                eyes.set_x(gaze_pixel[0])
                eyes.set_y(gaze_pixel[1])
                tasksPerFrame.checkIfFocused_ForAllTasks(eyes)

                # save results
                taskRecords.importTasksPerFrame(tasksPerFrame, frameNumber)
                taskRecords.drawTasks(dframe)
                timeline.importTasksPerFrame(tasksPerFrame, userId, startTimestamp_ms, cTimestamp_ms)

                # draw
                tasksPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
                tasksPerFrame.drawRect_ForAllTasks(dframe)
                tasksPerFrame.drawRectPoint_ForAllTasks(dframe)
                tasksPerFrame.drawID_ForAllTasks(dframe)
                ## arucos
                tasksPerFrame.drawArucos_ForAllTasks(dframe)
                tasksPerFrame.drawTaskElements_ForAllTasks(dframe)
                ## gaze point
                cv2.circle(dframe, (eyes.x, eyes.y), 10, (255, 0, 0), -1)  # visualisiere Eye-Tracking
                ## text
                cv2.putText(dframe, "fps=" + str(fps), (1920 - 500, 1080 - 10), 1, 1.4, (140, 22, 99), 2)
                drawControls(dframe)
                cv2.putText(dframe, "userId=" + str(userId), (1920 - 500, 1080 - 30), 1, 1.4, (140, 22, 99), 2)
                drawControls(dframe)
                cv2.imshow("frame", scaleImage(dframe, windowSize))

                # controls
                pressedKey = cv2.waitKey(1) & 0xFF
                if pressedKey == ord('q'):  # close
                    out_q.put(_sentinel)
                    cv2.destroyWindow("frame")
                    break

    except KeyboardInterrupt:
        pass
    finally:
        cv2.destroyAllWindows()


def openLiveWebcam(camIndex, videoRes, windowSize, userId, httpRequestPostUrl, out_q):
    # camera settings
    capture = cv2.VideoCapture(camIndex, cv2.CAP_DSHOW)
    xRes, yRes = videoRes
    capture.set(3, xRes)  # ID bei set für die Breite ist 3
    capture.set(4, yRes)
    res_x = int(capture.get(3))
    res_y = int(capture.get(4))
    if res_x == 0 and res_y == 0:
        tk.messagebox.showinfo("Camera Index", "No Camera connected at this index.")
        return

    eyes = Eyes()  # Die Augen sind per default in der Mitte der Bild-Aufnahme

    taskRecords = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
    timeline = TimelineElementsPerFrame(httpRequestPostUrl=httpRequestPostUrl, out_q=out_q)

    # timestamps
    startTimestamp_raw = time.time()  # in seconds but float
    startTimestamp_ms = int(startTimestamp_raw * 1000)

    # frames
    frameNumber = 0
    fps = 0
    prev_frame_time = 0
    frameTimeline = []

    while capture.isOpened:
        success, frame = capture.read()
        if not success:
            tk.messagebox.showinfo("Camera", "The camera is disconnected.")
            capture.release()
            cv2.destroyWindow("frame")
            return
        dframe = frame.copy()

        # Timestamps
        cTimestamp_raw = time.time()  # in seconds but float
        cTimestamp_ms = int(cTimestamp_raw * 1000)

        frameNumber += 1
        frameTimeline.append(frameNumber)

        cv2.circle(dframe, (int(eyes.x), int(eyes.y)), 10, (255, 0, 0), -1)  # visualisiere Eye-Tracking

        # QR-Scanner
        decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
        tasksPerFrame = TasksPerFrame(frameNumber)
        tasksPerFrame.decodeFrame(decodedFrame, cTimestamp_ms)

        # AruCo-Scanner
        tasksPerFrame.decodeArucoMarkers(frame, dframe)
        tasksPerFrame.assignArucosToTasks()
        tasksPerFrame.createTaskElementPerFrames_ForAllTasks()

        # Sind Tasks fokussiert?
        tasksPerFrame.checkIfFocused_ForAllTasks(eyes)

        # Speichere Ergebnisse
        taskRecords.importTasksPerFrame(tasksPerFrame, frameNumber)
        taskRecords.drawTasks(dframe)
        timeline.importTasksPerFrame(tasksPerFrame, userId, startTimestamp_ms, cTimestamp_ms)

        # Zeichne
        tasksPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
        tasksPerFrame.drawRect_ForAllTasks(dframe)
        tasksPerFrame.drawRectPoint_ForAllTasks(dframe)
        tasksPerFrame.drawID_ForAllTasks(dframe)

        tasksPerFrame.drawArucos_ForAllTasks(dframe)
        tasksPerFrame.drawTaskElements_ForAllTasks(dframe)

        cv2.putText(dframe, "fps=" + str(fps), (1920 - 500, 1080 - 10), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.putText(dframe, "userId=" + str(userId), (1920 - 500, 1080 - 30), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.imshow("frame", scaleImage(dframe, windowSize))

        # Controlls
        pressedKey = cv2.waitKey(1) & 0xFF
        if pressedKey == ord('q'):  # schließen
            out_q.put(_sentinel)
            cv2.destroyWindow("frame")
            break

        if pressedKey == ord('x'):  # createXML
            createXMLFromTaskRecords(taskRecords)

        def mousePoints(event, x, y, flags, params):
            if event == cv2.EVENT_LBUTTONDOWN:
                eyes.set_x(x / (windowSize / 100))
                eyes.set_y(y / (windowSize / 100))
                print(x, y)

        cv2.setMouseCallback("frame", mousePoints)

        # FPS
        new_frame_time = time.time()
        fps = 1 / (new_frame_time - prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)

    out_q.put(_sentinel)
    capture.release()


def openPupilDataFishEye2(pupilRecordingFolder, windowSize, userId, httpRequestPostUrl, out_q):
    # camera settings
    capture = cv2.VideoCapture(pupilRecordingFolder.get_iMotions_video_path())
    res_x = int(capture.get(3))
    res_y = int(capture.get(4))

    eyes = Eyes()  # Die Augen sind per default in der Mitte der Bild-Aufnahme
    gazeTLVFile = GazeTLVFile(pupilRecordingFolder.get_iMotions_tlv_path())
    xList = gazeTLVFile.get_column_by_name("Gaze2dX")
    yList = gazeTLVFile.get_column_by_name("Gaze2dY")
    timestampList = gazeTLVFile.get_column_by_name("GazeTimeStamp")
    startTimestamp_ms = pupilRecordingFolder.get_iMotions_info_dict()["Start Time (System)"]

    taskRecords = TaskRecords()  # hier drin werden die Daten (tasksPerFrame), die pro Frame erkannt werden, gespeichert.
    timeline = TimelineElementsPerFrame(httpRequestPostUrl=httpRequestPostUrl, out_q=out_q)

    # frames
    frameNumber = 0
    fps = 0
    prev_frame_time = 0
    frameTimeline = []

    while capture.isOpened:
        success, frame = capture.read()
        if not success:
            print("Video has ended.")
            capture.release()
            cv2.destroyWindow("frame")
            break
        dframe = frame.copy()

        # Frames
        frameNumber += 1
        frameTimeline.append(frameNumber)

        # Eyes
        xPos = xList[frameNumber]
        yPos = yList[frameNumber]
        eyes.set_x(xPos)
        eyes.set_y(yPos)
        cv2.circle(dframe, (int(eyes.x), int(eyes.y)), 10, (255, 0, 0), -1)  # visualize eye-gaze

        # QR-Scanner
        decodedFrame = decode(frame, symbols=[ZBarSymbol.QRCODE])
        taskListPerFrame = TasksPerFrame(frameNumber)
        taskListPerFrame.decodeFrame(decodedFrame,
                                     pupilRecordingFolder.convert_pupil_time_to_system_time(timestampList[frameNumber]))

        # AruCo-Scanner
        taskListPerFrame.decodeArucoMarkers(frame, dframe)
        taskListPerFrame.assignArucosToTasks()
        taskListPerFrame.createTaskElementPerFrames_ForAllTasks()

        # Sind Tasks fokussiert?
        taskListPerFrame.checkIfFocused_ForAllTasks(eyes)

        # Speichere Ergebnisse
        taskRecords.importTasksPerFrame(taskListPerFrame, frameNumber)
        taskRecords.drawTasks(dframe)
        cTimestamp_ms = pupilRecordingFolder.convert_pupil_time_to_system_time(timestampList[frameNumber])
        timeline.importTasksPerFrame(taskListPerFrame, userId, startTimestamp_ms, cTimestamp_ms)

        # Zeichne
        taskListPerFrame.drawRecsQrCodes_ForAllTasks(dframe)
        taskListPerFrame.drawRect_ForAllTasks(dframe)
        taskListPerFrame.drawRectPoint_ForAllTasks(dframe)
        taskListPerFrame.drawID_ForAllTasks(dframe)

        taskListPerFrame.drawArucos_ForAllTasks(dframe)
        taskListPerFrame.drawTaskElements_ForAllTasks(dframe)

        cv2.putText(dframe, "fps=" + str(fps), (1920 - 500, 1080 - 10), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.putText(dframe, "userId=" + str(userId), (1920 - 500, 1080 - 30), 1, 1.4, (140, 22, 99), 2)
        drawControls(dframe)
        cv2.imshow("frame", scaleImage(dframe, windowSize))

        # Controlls
        pressedKey = cv2.waitKey(1) & 0xFF
        if pressedKey == ord('q'):  # schließen
            out_q.put(_sentinel)
            cv2.destroyWindow("frame")
            break

        if pressedKey == ord('x'):  # createXML
            createXMLFromTaskRecords(taskRecords)

        def mousePoints(event, x, y, flags, params):
            if event == cv2.EVENT_LBUTTONDOWN:
                eyes.set_x(x / (windowSize / 100))
                eyes.set_y(y / (windowSize / 100))
                print(x, y)

        cv2.setMouseCallback("frame", mousePoints)

        # FPS
        new_frame_time = time.time()
        fps = 1 / (new_frame_time - prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)

    out_q.put(_sentinel)
    capture.release()
